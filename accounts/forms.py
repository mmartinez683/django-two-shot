from django import forms


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput(), max_length=150)


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput(), max_length=150)
    password_confirmation = forms.CharField(
        widget=forms.PasswordInput(), max_length=150
    )
