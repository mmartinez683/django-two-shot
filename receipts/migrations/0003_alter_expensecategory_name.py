# Generated by Django 4.2 on 2023-04-19 17:40

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_expensecategory_owner"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expensecategory",
            name="name",
            field=models.CharField(max_length=50),
        ),
    ]
